$( document ).ready(function() {
    checkSession();
    getUserPermissions();
    $('[data-toggle="tooltip"]').tooltip()
});
window.onload = closeButton;

/* Check that the session is valid */
function checkSession() {
    var res;
    $.getJSON( "/api/check_session", function( response ) {
        res = response.is_valid_session;
    }).done(function() {
        if (!res) {
            $('#nologModal').modal('show');
        }
    }).fail(function() {
        console.log('failed to check session.');
    });
}
/* Get user permissions */
function getUserPermissions() {
    var p = localStorage.getItem('permissions');
    if (p == null){
        $.ajax({
            url: "/api/permissions",
            dataType: "json",
            success: function (data) {
                p = data.is_admin;
            },
            error: function (err) {
                console.log(err);
            }
        }).done( function() {
            if(p == true) 
                $(".admin").show();
            else 
                $(".admin").hide();
            localStorage.setItem('permissions', p);
        });
    } else if (p == true) {
        $(".admin").show();
    } else if (p == false) {
        $(".admin").hide();
    }
}
/* Clear local storage on logout */
function userLogout() {
    localStorage.clear();
    sessionStorage.clear();
    $.ajax({
        url: "/api/logout",
        type: "GET"
    }).done ( function() {
        window.location = "/";
    });
}

/* Get the week dates, and add to sidebar/tables */
function getAllWeeks() {
    $.ajax({
        url: "/api/calendarDates",
        dataType: "json",
        success: function (data) {
            var todaysDate = new Date().toJSON().slice(0,10).replace(/-/g,'-');
            $.each( data.weeks, function( i, week ) {
                start = new Date(week.start);
                startStr = start.toDateString().split(' ');
                end = new Date(week.end);
                endStr = end.toDateString().split(' ');
                //SET the CURRENT WEEK as ACTIVE NAV PILL
                var s = startStr[1] + " " + startStr[2] + ((startStr[3] != endStr[3]) ? " " + startStr[3] : "")
                + " - " + ((startStr[1] != endStr[1]) ? endStr[1] + " " : "") + endStr[2] + " " + endStr[3];

                id = "week" + ((week.groupid < 3) ? week.groupid - 3 : week.groupid - 2) + "Date";
                var elements = document.getElementsByClassName(id);
                for(var i=0; i<elements.length; i++) {
                    elements[i].innerHTML = elements[i].innerHTML + "<small>" + s + "</small>"; //&nbsp
                }
            });
        },
        error: function (err) {
            console.log(err);
        }
    });
};
/* update the week from-to html elements and DB */
function updateWeekDates(weekID) {
    var id = (weekID < 3) ? weekID - 3 : weekID - 2;
    var start = $("#exampleDropdownFormEmail" + id).val()
    var startDate = new Date(start + "T12:00:00-06:00");
    if(startDate.getDay() == 1){
        var end = new Date(startDate.valueOf()+(4*24*60*60*1000));
        endStr = end.toDateString().split(' ');
        startStr = startDate.toDateString().split(' ');
        var s = startStr[1] + " " + startStr[2] + ((startStr[3] != endStr[3]) ? " " + startStr[3] : "")
                + " - " + ((startStr[1] != endStr[1]) ? endStr[1] + " " : "") + endStr[2] + " " + endStr[3];

        var elements = document.getElementsByClassName("week" + id + "Date");
        var beforeStr = "";
        for(var i=0; i<elements.length; i++) {
            var inside = ((elements[i].innerHTML.replace("/small","").replace("small","").replace("<>","").replace("<>","")).valueOf()).split(": ");
            var i = (inside.length > 1) ? (inside[1] + ": ") : inside[0];
            if(i === (s+": "))
                alert("Pick a new date; " + i + " is the current date.");
            else {
                elements = document.getElementsByClassName("week" + id + "Date");
                for(var i=0; i<elements.length; i++) {
                    elements[i].innerHTML = beforeStr + "<small>" + s + "</small>";
                }
                //update DB start_date end_date id
                var endDateDB = end.toISOString().split('T');
                var formData = '{ "id":"'+ weekID +'", "start_date":"'+ start +'", "end_date":"'+ endDateDB[0] +'" }';
                formData = JSON.parse(formData);
                console.log(formData);
                $.ajax({
                    url: "/api/updateCalDate",
                    dataType: "json",
                    data: formData,
                    type: 'POST',
                    success: function ( response ) {},
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
        }

    } else {
        alert("You have to pick a Monday as the start date!");
    }
}

/* Pull Labs from DB, style, and add to week */
function getAllLabs() {
    var labs;
    $.ajax({
        url: "/api/calendar",
        dataType: "json",
        success: function ( response ) {
            labs = response.results;
        },
        error: function (err) {
            console.log(err);
        }
    }).done( function () {
        $.each( labs, function( i, lab ) {
            //black=saved, red=published
            var labID = (lab.groupid < 3) ? lab.groupid - 3 : lab.groupid - 2;
            var id = "#week" + labID + "LabTable";
            console.log(id);
            var tableHTML;
            var trColor = "";
            var titleColor;
            var pub = "";
            if(lab.color === 'red') {
                titleColor = "labTitle";
            } else {
                titleColor = "lab-saved-color";
                trColor = " class='lab-saved-bg'";
                pub = "(SAVED) ";
            }
            tableHTML = "<tr" + trColor + "><div><th scope='row' style='text-align: center;'><a href='" 
                    + lab.url + "' style='display: inline-flex'><p class='" 
                    + titleColor + "'><i class='fa fa-arrow-right'></i>&nbsp;"
                    + lab.title + "</p></a></th><td><p>" + pub 
                    + lab.description + "</p></td></div></tr>";
            $(id).append(tableHTML);
        });
    });
}

/* Add Lab Date select option set to week from which the add lab was selected */
function addLabSetWeekID(weekID){
    document.getElementById("dd-"+weekID).selected = "true";
}
/* Create Lab using Add Lab Button */
function create_lab() {
    var second_time = sessionStorage.getItem("second_time");
    if(second_time == null){
        sessionStorage.setItem("second_time", false);
        second_time = false;
    }
    event.preventDefault(); // To prevent following the link (optional)
    if (second_time == true) {
        sessionStorage.setItem("second_time", false);
        document.getElementById("upload_area").style.display = "none";
        document.getElementById("form_area").style.display = "block";
        $('#addLabModal').modal('hide');
    } else {
        data = {week_id: $('#labStartDate').children("option:selected").val(), title: $('#labTitle').val(), description: $('#labDescription').val(), published: true};
        $.ajax({ 
            url: "/api/lab/create",
            data: data,
            type: 'POST',
            xhrFields: {withCredentials: true},
        }).done(function (data) {
            document.getElementById("lab_id").value = data.id;
        });
        document.getElementById("upload_area").style.display = "block";
        document.getElementById("form_area").style.display = "none";
        document.getElementById("create_lab").text = "Create";
        sessionStorage.setItem("second_time", true);
    }
};
/* Resets Add Lab button */
function restartCreateLab() {
    sessionStorage.setItem("second_time", false);
    document.getElementById("upload_area").style.display = "none";
    document.getElementById("form_area").style.display = "block";
}
/* Delete File in Lab Page - Check User Agrees with Delete */
function delete_item(hash) {
    console.log(hash);
    sessionStorage.setItem("delete_file", hash);
    $('#deleteWarning').modal('show');
}
/* Delete File in Lab Page */
function delete_agree (){
   var hash = sessionStorage.getItem("delete_file");
   document.getElementById(hash).style.display = "none";
    $.ajax({
        url: "/api/file",
        type: "DELETE",
        data: {
            "id": hash,
        }
    }).done( function() {
       $('#deleteWarning').modal('hide');
       sessionStorage.removeItem('delete_file');
    });
}


/* TO Do List Task Remove Button to existing To Do List Tasks */
function closeButton() {
    var close = document.getElementsByClassName("close-list");
    var i;
    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var content = this.parentElement;
            if (content.hasAttribute("data-dismiss")) {
                console.log();
            } else {
                var item = content.parentElement;
                var list = item.parentElement;
                list.removeChild(item);
            }
        }
    }
}
/* Add Remove To Do List Tasks */
function newTaskElement(listID) {
    var myDiv = document.getElementById(listID+"ToDoList");

    var a = document.createElement('a');
    a.href = "#";
    a.setAttribute('class', 'list-group-item list-group-item-action flex-column align-items-start');

    var list = document.createElement('div');
    list.setAttribute('class', 'd-flex w-100 justify-content-between');

    var elemnt = document.createElement('div');
    elemnt.setAttribute('class', 'form-check form-check-inline');

    // creating checkbox element
    var checkbox = document.createElement('input');
    // Assigning the attributes to created checkbox
    checkbox.type = "checkbox";
    checkbox.id = "id";
    checkbox.setAttribute('class', 'form-check-input');

    // creating label for checkbox
    var label = document.createElement('label');
    // assigning attributes for the created label tag
    label.htmlFor = "id";
    label.setAttribute('class', 'strikethrough');

    var inputValue = document.getElementById(listID+"Task").value;
    var header = document.createElement("h6");
    header.setAttribute('class', 'mb-1');
    header.appendChild(document.createTextNode(inputValue));

    label.appendChild(header);

    // appending the checkbox and label to div
    elemnt.appendChild(checkbox);
    elemnt.appendChild(label);
    list.appendChild(elemnt);
    a.appendChild(list);

    if (inputValue === '') {
        alert("You must write something!");
    } else {
        myDiv.appendChild(a);
    }
    document.getElementById(listID+"Task").value = "";

    var span = document.createElement("SPAN");
    span.setAttribute('class', 'admin close close-list');
    span.appendChild(document.createTextNode("\u00D7"));
    list.appendChild(span);

    var close = document.getElementsByClassName("close-list");
    var i;
    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var content = this.parentElement;
            var item = content.parentElement;
            var list = item.parentElement;
            list.removeChild(item);
            //ADD Remove Task from To-Do List / Remove Document
        }
    }
    //ADD Add Task to To-Do List / Add Document
}