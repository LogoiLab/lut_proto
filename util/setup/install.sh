#!/bin/bash
####################################################
# Lab Update Tool Installer Script.                #
####################################################
# Author(s): Chad Baxter, John Evans, Kristin Frye #
# Date:      2020-04-07                            #
# URL:       https://gitlab.com/LogoiLab/lut_proto #
####################################################
if [[ $(id -u) -ne 0 ]]; then
    exec sudo $0
fi

mkdir install;
pushd install;
apt-get install postgresql wget nodejs git;
# Get the project
git clone https://gitlab.com/LogoiLab/lut_proto.git;
# Start postgres
systemctl enable postgresql;
systemctl start postgresql;
# Setup postgres
pushd lut_proto;
cp db.sql /tmp/;
popd;
su postgres;
psql -f /tmp/db.sql;
exit;
rm /tmp/db.sql;
# Install IPFS
wget https://dist.ipfs.io/go-ipfs/v0.4.9/go-ipfs_v0.4.9_linux-amd64.tar.gz;
tar -xvf go-ipfs_v0.4.9_linux-amd64.tar.gz;
pushd go-ipfs;
./install.sh;
popd;
# Create IPFS daemon
useradd ipfs -m -s /bin/nologin;
popd;
mkdir /home/ipfs/.config;
mkdir /home/ipfs/.config/systemd;
mkdir /home/ipfs/.config/systemd/user;
pushd /home/ipfs/.config/systemd/user;
echo "[Unit]
Description=IPFS daemon
After=network.target

[Service]
ExecStart=/usr/local/bin/ipfs daemon --enable-pubsub-experiment --enable-namesys-pubsub
Restart=on-failure

[Install]
WantedBy=default.target" > ipfs.service;
su ipfs;
systemctl --user enable ipfs;
systemctl --user start ipfs;
exit;
pushd lut_proto;
npm start --harmony-async-iteration &
