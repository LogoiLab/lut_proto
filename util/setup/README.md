# LUT Setup Scripts

## install.sh
Do not use this script. Use the ansible script instead.

## Ansible Playbook

Required AWS instance OS: **Ubuntu 18.04 LTS**

Install ansible according to this guide: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

Edit the following variables located in `./ansible/vars/default.yml`:

1. `key_path`: The location of the AWS SSH key to use.
2. `domain`: The domain you intend to run LUT at.
3. `db_password`: Set this the same as the db_config.json file in the project root.

Before running the ansible playbook, be sure you have an ECDSA ssh key in the current user's `.ssh` folder. This key will be used for administering the server under the account `administer`. If you would like to use a different key replace the path inside the `lookup()` in the variable `copy_local_key`.

Edit the `inventory.ini` file and replace the current IP with the public IP of your fresh AWS instance like so:

```
[servers]
server ansible_host=<AWS instance IP here>
```

Ensure your current directory is `./ansible` then run the following command:

`ansible-playbook playbook.yml -i inventory.ini`
