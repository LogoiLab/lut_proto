/*const { Pool, Client } = require('pg');
const pool = new Pool ({
    user: 'lut_proto',
    host: 'localhost',
    database: 'lut_proto',
    password: 'lut_protopass',
    port: 5432
});

module.exports = pool;*/

var fs = require('fs');
var config = JSON.parse(fs.readFileSync('./db_config.json', 'utf8'));

const { Pool, Client } = require('pg');
const pool = new Pool ({
    user: config.user,
    host: config.host,
    database: config.database,
    password: config.password,
    port: config.port
});

module.exports = pool;
