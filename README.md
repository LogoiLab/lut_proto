# LUT_PROTO

## A prototype lab update tool

Installation and running steps.

1. Clone the repo
2. Install nodejs, npm, ipfs and postgres.
3. Run the `db.sql` file against your postgres instance.
4. Open a terminal and run `ipfs daemon` (after following all the steps in the [ipfs documentation](https://ipfs.io))
5. Open a terminal with the current working directory of the repo.
6. Run `npm update`
7. Run `DEBUG=lut_proto:* npm start --harmony-async-iteration`
8. Open a browser and browse to `http://localhost:3000`
9. That's it. Log into the application with the password `testpassword`
