-- CREATE USER lut_proto WITH PASSWORD 'lut_protopass'; <<<---OLD DONT USE
-- CREATE DATABASE lut_proto;

\c lut_proto

GRANT ALL PRIVILEGES ON DATABASE lut_proto TO lut_proto;

CREATE TABLE users (
id SERIAL PRIMARY KEY,
password TEXT NOT NULL,
logged_in BOOLEAN NOT NULL,
session_key TEXT,
session_timeout TIMESTAMPTZ,
);
GRANT ALL PRIVILEGES ON users TO lut_proto;
INSERT INTO users (password, logged_in) VALUES ('$2b$10$cW8GiVs3EFtFrl3LgtoAXOch0dIJCh9G6VDq4XEfJ92urf5qXJjsO', false); -- pass is 'testpassword' encrypted with bcrypt and 10 rounds of salt.

CREATE TABLE permissions (
user_id INTEGER REFERENCES users(id) NOT NULL,
is_admin BOOLEAN NOT NULL
);
GRANT ALL PRIVILEGES ON permissions TO lut_proto;
INSERT INTO permissions (user_id, is_admin) VALUES (1, true); -- make the first user the default admin.

CREATE TABLE weeks (
id SERIAL PRIMARY KEY,
title TEXT,
start_date DATE NOT NULL,
end_date DATE NOT NULL
);
GRANT ALL PRIVILEGES ON weeks TO lut_proto;

CREATE TABLE labs (
id SERIAL PRIMARY KEY,
week_id INTEGER REFERENCES weeks(id) NOT NULL,
title TEXT NOT NULL,
description TEXT,
published BOOLEAN NOT NULL
);
GRANT ALL PRIVILEGES ON labs TO lut_proto;

CREATE TABLE files (
id TEXT PRIMARY KEY,
name TEXT NOT NULL,
lab_id INTEGER REFERENCES labs(id) NOT NULL,
);
GRANT ALL PRIVILEGES ON files TO lut_proto;

CREATE TABLE lists (
id SERIAL PRIMARY KEY,
user_id INTEGER REFERENCES users(id) NOT NULL,
week_id INTEGER REFERENCES weeks(id) NOT NULL
);
GRANT ALL PRIVILEGES ON lists TO lut_proto;

CREATE TABLE tasks (
id SERIAL PRIMARY KEY,
list_id INTEGER REFERENCES lists(id) NOT NULL,
file_id TEXT REFERENCES files(id),
title TEXT NOT NULL,
description TEXT,
completed BOOLEAN
);
GRANT ALL PRIVILEGES ON tasks TO lut_proto;
