var express = require('express'); //duh
var router = express.Router(); //init a router
var bcrypt = require('bcrypt'); //password hashing
var db = require('../util/database.js'); //global postgres pool
var { v4: uuid } = require('uuid'); //unique session key generator

// calendar events endpoint mounted at /api/calendar
router.get('/api/calendar', async function(req, response) {
    try {
    //const query = "SELECT session_key FROM users WHERE id = $1";
    //const res = await db.query(query, [1]); // hardcoded user id for testing
    //if(req.session.session_key == res.rows[0].session_key && res.rows[0].session_key != null) {
        const permsQuery = "SELECT * FROM permissions WHERE user_id = $1";
        const permsResp = await db.query( permsQuery, [1] ); //hardcoded as user ID #1
        var calendarQuery;
        if(permsResp.rows[0].is_admin == true) {
            calendarQuery = "select "
                + "w.id as groupId, "
                + "w.start_date as start, "
                + "w.end_date as end, "
                + "'true' as allDay, "
                + "CONCAT('/lab.html?id=',l.id) as url, "
                + "l.title, "
                + "CASE WHEN l.published='t' THEN 'red' ELSE 'black' END AS color, "
                + "l.description "
                + "from labs l join weeks w on w.id=l.week_id;";
        } else {
            calendarQuery = "select "
                + "w.id as groupId, "
                + "w.start_date as start, "
                + "w.end_date as end, "
                + "'true' as allDay, "
                + "CONCAT('/lab.html?id=',l.id) as url, "
                + "l.title, "
                + "'red' as color, "
                + "l.description "
            + "from labs l join weeks w on w.id=l.week_id"
            + "where l.published='t';";
        }
        const calendarResp = await db.query( calendarQuery, [] );
        //change group ID - order items by date, group by similar date and count
        if(calendarResp.rowCount > 0){
            const results = calendarResp.rows.map(function(item) {
                return item;
            });
            response.json({ results });

        }  else {
            response.json([]);
        }
    } catch(_) {
        response.json([]);
    }
});

router.get('/api/calendarDates', async function(req, response) {
    try {
        var calendarQuery = "SELECT '0' as groupid, w.start_date as start, w.end_date as end from weeks w order by w.start_date ASC;";
        const calendarResp = await db.query( calendarQuery, []);
        var count = 1;
        var startDateCurrent;
        if((calendarResp.rowCount) > 0){
            var startDatePrev = new Date(calendarResp.rows[0].start);
            console.log(startDatePrev);
            const weeks = calendarResp.rows.map(function(item) {
                startDateCurrent = new Date(item.start);
                if((startDateCurrent - startDatePrev) > 0){
                    count++;
                    startDatePrev = startDateCurrent;
                }
                item.groupid = count;
                return item;
            });
            response.json({ weeks });
        } else {
            response.json([]);
        }
    } catch(_) {
        response.json([]);
    }
});

router.post('/api/updateCalDate', async function(req, response) {
    try {
        var calendarQuery = "UPDATE weeks SET start_date = $1, end_date = $2 WHERE id = $3;";
        const calendarResp = await db.query( calendarQuery, [req.body.start_date, req.body.end_date, req.body.id]);
        console.log(calendarResp.rowCount);
        if((calendarResp.rowCount) > 0){
            console.log("Date Updated in DB");
            response.json([]);
        } else {
            console.log("Date was not Updated in DB");
            response.json([]);
        }
    } catch(_) {
        console.log("An Error Occured while trying to Update Date in DB");
        response.json([]);
    }
});

module.exports = router;
