var express = require('express'); //duh
var router = express.Router(); //init a router
var bcrypt = require('bcrypt'); //password hashing
var db = require('../util/database.js'); //global postgres pool
var { v4: uuid } = require('uuid'); //unique session key generator

// main api endpoint.
router.get('/api', function(req, res) {
  res.send(''); //silence is golden
});
router.post('/api', function(req, res) {
    res.send(''); //silence is golden
});

function formatDate(date) {
    var year = date.getFullYear(),
        month = date.getMonth() + 1, // months are zero indexed
        day = date.getDate(),
        hour = date.getHours(),
        minute = date.getMinutes(),
        second = date.getSeconds(),
        minuteFormatted = minute < 10 ? "0" + minute : minute;

    return hour + ":" + minuteFormatted + ":" + second;
}

// session checker.
router.get('/api/check_session', async function(req, response) {
    try {
        const query = "SELECT session_key, logged_in, EXTRACT(EPOCH FROM session_timeout) FROM users WHERE id = $1";
        const res = await db.query(query, [1]); // hardcoded user id for testing
        var time = new Date();
        //console.log(res.rows[0].date_part < (time.getTime() + 120000) / 1000);
        //console.log((time.getTime() + 120000) / 1000);
        if(res.rows[0].logged_in && req.session.session_key == res.rows[0].session_key && res.rows[0].date_part < (time.getTime() + 120000) / 1000) {
            //console.log("in if");
            const query = "UPDATE users SET session_timeout = now()";
            const res = await db.query(query, []);
            response.json({is_valid_session: true});
        }else{
            const query = "UPDATE users SET session_timeout = current_date, logged_in = false, session_key='' WHERE id = $1";
            const res = await db.query(query, [1]);
            response.json({is_valid_session: false});
        }
    } catch (e) {
        console.log(e);
        response.json({is_valid_session: false});
    }
});

// login handler.
router.post('/api/login', async function(req, response) {
    bcrypt.hash('testpassword', 10, async function(err, hash) {
        //ensure testing password is always set to what it should be. remove this later
        const query = "UPDATE users SET password = $1 WHERE id = $2";
        const res = await db.query(query, [hash, 1]);
    });
    const query = "SELECT password FROM users WHERE id = $1";
    const res = await db.query(query, [1]); //hardcoded user id. replace with username later
    //console.log(res.rows[0].password);
    //console.log(req.body.password);
    bcrypt.compare(req.body.password, res.rows[0].password, async function(err, result) {
        //console.log(result);
        if(result) { // test hashed password
            const query = "SELECT logged_in FROM users WHERE id = $1";
            const res = await db.query(query, [1]);
            //console.log(res.rows[0].logged_in);
            if(res.rows[0].logged_in) {
                response.end('already_loggedin');
            } else {
                try {
                    req.session.user_id = 1; //set user id
                    var sess_key = uuid(); //generate custom session key
                    const query = "UPDATE users SET logged_in = true, session_key = $1, session_timeout = now() WHERE id = $2";
                    var time = new Date();
                    const res = await db.query(query, [sess_key, 1]); //add key to db
                    req.session.session_key = sess_key; //set the key in the session
                } catch (e) {
                    console.log(e);
                    response.end('failed_to_create_session');
                }
                response.redirect('/dashboard.html');
            }
        } else {
            response.end('failed_login');
        }
    });
});
// logout handler
router.get('/api/logout', async function(req, response) {
    try{
        const query = "UPDATE users SET logged_in = false, session_key = null WHERE session_key = $1";
        //update user in database
        const res = await db.query(query, [req.session.session_key]);
        req.session.destroy(); //destroy the session to prevent cookie reuse.
    } catch (_) {
        response.end('failed to log out');
    }
    response.redirect('/');
});

// permission for show, hide elements
router.get('/api/permissions', async function(req, response) {
    try{
        const permsQuery = "SELECT * FROM permissions WHERE user_id = $1";
        const permsResp = await db.query( permsQuery, [1] ); //hardcoded as user ID #1
        if(permsResp.rows[0].is_admin == true) {
            response.json({is_admin: true});
        } else {
            response.json({is_admin: false});
        }
    } catch (_) {
        response.json({is_admin: false});
    }
});

module.exports = router;
