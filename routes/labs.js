var express = require('express'); //duh
var router = express.Router(); //init a router
var bcrypt = require('bcrypt'); //password hashing
var db = require('../util/database.js'); //global postgres pool
var { v4: uuid } = require('uuid'); //unique session key generator

// lab retrieval endpoint
router.get('/api/lab', async function(req, response) {
    try {
        if (req.query.id == 'null') {
            response.json([]);
        }
        const query = "SELECT * FROM labs WHERE id = $1";
        const res = await db.query(query, [req.query.id]);
        response.json(res.rows[0]);
    } catch(_) {
        response.end();
    }
});

// Initial lab creation endpoint
router.post('/api/lab/create', async function(req, response) {
    //console.log(req.body.week_id);
    //console.log(req.body.title);
    //console.log(req.body.description);
    //console.log(req.body.published);
    //console.log(req);
    try {
        const query = "INSERT INTO labs (week_id, title, description, published) VALUES ($1, $2, $3, $4)";
        const res = await db.query(query, [Number(req.body.week_id), req.body.title, req.body.description, Boolean(req.body.published)]);
        /*Create & Open
          const url = "SELECT CONCAT('/lab.html?id=',l.id) as url from labs l where week_id=$1 and title=$2";
          const urlResp = await db.query(url, [req.body.week_id, req.body.title]);
          if(urlResp.rowCount > 0) {
          response.redirect(urlResp);
          }*/
        response.json({"status": "success"});
    } catch(e) {
        response.send(e);
        //response.json({"status": "failure"});
    }
});

// Lab updating endpoint
router.post('/api/lab/update', async function(req, response) {
    try {
        const query = "UPDATE labs SET title = COALESCE($1, title), description = COALESCE($2, description), published = COALESCE($3, published) WHERE id = $4 AND  ($1 IS NOT NULL AND $1 IS DISTINCT FROM title OR $2 IS NOT NULL AND $2IS DISTINCT FROM description OR $3 IS NOT NULL AND $3 IS DISTINCT FROM published)";
        const res = await db.query(query, [req.body.title, req.body.description, req.body.published, req.body.id]);
        response.json({
            status: success,
            id: 3
        });
    } catch(_) {
        response.end('{status: failure}');
    }
});

// Lab deletion endpoint
router.delete('/api/lab', async function(req, response) {
    try {
        const query = "DELETE FROM labs WHERE id = $1";
        const res = await db.query(query, [req.body.id]);
        response.json({status: success});
    } catch(_) {
        response.end('{status: failure}');
    }
});

module.exports = router;
