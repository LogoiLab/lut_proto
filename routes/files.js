var express = require('express'); //duh
var router = express.Router(); //init a router
var bcrypt = require('bcrypt'); //password hashing
var db = require('../util/database.js'); //global postgres pool
var { v4: uuid } = require('uuid'); //unique session key generator
const IpfsHttpClient = require('ipfs-http-client');
const { globSource } = IpfsHttpClient;
const ipfs = IpfsHttpClient("/ip4/127.0.0.1/tcp/5001");
const CID = require('cids');
const fs = require('fs');

// USE THIS FOR FRONTEND: https://www.dropzonejs.com/

// HOW TO DO FILE UPLOAD: http://howtonode.org/really-simple-file-uploads

// calendar events endpoint mounted at /api/calendar
router.get('/api/file', async function(req, response) {
    try {
        const query = "SELECT id, name FROM files WHERE lab_id = $1";
        const res = await db.query(query, [req.query.lab_id]);
        response.json(res.rows);
    } catch(_) {
        response.json([]);
    }
});

router.delete('/api/file', async function(req, response) {
    try {
        const query = "DELETE FROM files WHERE id = $1";
        const res = await db.query(query, [req.query.file_id]);
        await ipfs.pin.rm(req.query.file_id);
        response.json({"status": "success"});
    } catch(_) {
        response.json({"status": "failure"});
    }
});

router.post('/api/file', async function(req, response) {
    if (!req.files || Object.keys(req.files).length === 0) {
        return response.status(400).send('No files were uploaded.');
    }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let upload_file = req.files.file;

    // Use the mv() method to place the file somewhere on your server
    var tpath =  uuid();
    const files = await ipfs.add([{path: upload_file.name, content: upload_file.data}], {wrapWithDirectory: true});
    //console.log(files);
    for await (const file of files) {
        console.log(file.path);
        if (file.path == ''){
        try {
            console.log(req.body.lab_id);
                const query = "INSERT INTO files (id, name, lab_id) VALUES ($1, $2, $3)";
            const res = await db.query(query, [file.cid.toString(), upload_file.name, Number(req.query.lab_id)]);
                console.log(res);
                return response.send('File uploaded!');
            } catch (e) {
                console.log(e);
                return response.send(e);
            }
       }
    }
    //   return response.send("fail");
    return response.status(200);
});

module.exports = router;
